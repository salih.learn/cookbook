#include <iostream>
#include <list>
using namespace std;

int main() {
    list<int> L;
    for(int i=0;i<5;i++){
        L.push_back(i);
    }
    for(int i=5;i<10;i++){
        L.push_front(i);
    }
    list<int>::iterator center = L.begin();
    advance(center, (int) L.size() / 2);
    L.insert(center, 11);
    for(list<int>::iterator i = L.begin(), end = L.end(); i != end; advance(i, 1)){
        cout << *i << " ";
    }
    return 0;
}
